<?php
/**
 * My Plugin
 *
 * @package     PluginPackage
 * @author      Your Name
 * @copyright   2016 Your Name or Company Name
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: My Plugin
 * Plugin URI:  https://example.com/plugin-name
 * Description: Description of the plugin.
 * Version:     1.0.0
 * Author:      Your Name
 * Author URI:  https://example.com
 * Text Domain: my-plugin
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

add_action( 'admin_menu', 'my_plugin_menu');

function my_plugin_menu(){
	$page_title = 'My Plugin';
	$menu_title = 'My Plugin';
	$capability = 'manage_options';
	$menu_slug = 'my-plugin-welcome';
	$function = 'welcome';

	add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function );
}

function welcome(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<p>Welcome.</p>';
	echo '</div>';
}

//for submenu
add_action( 'admin_menu', 'my_plugin_submenu');

function my_plugin_submenu(){
	$parent_slug = 'my-plugin-welcome';
	$page_title = 'My Plugin Submenu';
	$menu_title = 'My Plugin Submenu';
	$capability = 'manage_options';
	$menu_slug = 'my-plugin-submenu';
	$function = 'my_submenu';

	add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
}

function my_submenu(){
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	echo '<div class="wrap">';
	echo '<p>Here is where the form would go if I actually had options.</p>';
	echo '</div>';
}

//now let's create a custom post type under our newly created plugin

function create_post_type(){

	$post_type = 'mypost';
	$args = array(
		'labels' => array('name' => __('My Posts'),
							'singular_name' => __('My Post')
		),
		'public' => true,
		'rewrite' => array('slug' => 'my-posts'),
		'show_in_menu' => 'my-plugin-welcome',
		'show_in_admin_bar' => 'my-plugin-welcome',
		'capability_type' => 'page',
	);

	register_post_type( $post_type, $args);
}

add_action('init', 'create_post_type');



